const express = require('express');
const router = express.Router();
const { Item } = require('../models/item');
const { User } = require('../models/user');

// Add Route
router.get('/add', ensureAuthenticated, async (req, res) => {
  res.render('add_item', {
    title: 'Add Item'
  });
});

// Add Submit POST Route
router.post('/add', async (req, res) => {
  try {
    req.checkBody('title', 'Title is required').notEmpty();
    req.checkBody('body', 'Body is required').notEmpty();

    // Get Errors
    let errors = req.validationErrors();

    if (errors) {
      res.render('add_item', {
        title: 'Add Item',
        errors: errors
      });
    } else {
      let item = await Item.create({
        title: req.body.title,
        author: req.user._id,
        body: req.body.body,
      });
      item.save();
      req.flash('success', 'Item Added');
      res.redirect('/');
    }
  } catch (e) {
    res.send(e);
  }

});

// Load Edit Form
router.get('/edit/:id', ensureAuthenticated, async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    if (item.author != req.user._id) {
      req.flash('danger', 'Not Authorized');
      return res.redirect('/');
    }
    res.render('edit_item', {
      title: 'Edit Item',
      item: item
    });

  } catch (e) {
    res.send(e);
  }

});

// Update Submit POST Route
router.post('/edit/:id', async (req, res) => {
  try {
    const item = {
      title: req.body.title,
      author: req.body.name,
      body: req.body.body
    };

    let query = { _id: req.params.id }

    const update = await Item.update(query, item);
    if (update) {
      req.flash('success', 'Item Updated');
      res.redirect('/');
    } return;

  } catch (e) {
    res.send(e);
  }

});

// Delete Item
router.delete('/:id', async (req, res) => {

  try {
    if (!req.user._id) {
      res.status(500).send();
    }
    let query = { _id: req.params.id }
    const item = await Item.findById(req.params.id);

    if (item.author != req.user._id) {
      res.status(500).send();
    } else {
      remove = await Item.findByIdAndRemove(query);
      if (remove) {
        res.send('Success');
      }
    };
  } catch (e) {
    res.send(e);
  }

});



// Get Single Item
router.get('/:id', async (req, res) => {

  const item = await Item.findById(req.params.id);
  const user = await User.findById(item.author);
  if (user) {
    res.render('item', {
      item: item,
      author: user.name
    });
  }
});

// Access Control
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Please login');
    res.redirect('/users/login');
  }
}

module.exports = router;
