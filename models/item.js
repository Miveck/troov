const mongoose = require('mongoose');

// Item Schema
const itemSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  }
});
const Item = mongoose.model('Item', itemSchema);
module.exports.Item = Item;
